#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include <ctime>
#include <algorithm>

using namespace std;

const double pi=3.14159265359;
const double mp=938.2720813e6;// eV

// Function to scan the 3D tabulated Green function (include also the z-direction)
double func_fE0_3D(double fE_data[], double r, double z, double t, int cord_E, double rmin, double zmin, double tmin, double dlogr, double dlogz, double dlogt, int Nr, int Nz, int Nt, int NE){// Input t in yr, and s0 in pc
    
    int cord_r, cord_z, cord_t;
    
    if(r<rmin){
        r=rmin;
    }

    if(t<tmin){
        t=tmin;
    }

    cord_r=int(log10(r/rmin)/dlogr);
    cord_t=int(log10(t/tmin)/dlogt);
    if(abs(z)<zmin){
        z=zmin;
    }

    double r1, r2, z1, z2, t1, t2;

    if(z>0.0){
        cord_z=(Nz/2)+int(log10(z/zmin)/dlogz);
        if(cord_z>=Nz-1){
            cord_z=Nz-2;
        }
        z1=zmin*pow(10.0,(abs(cord_z-(Nz/2)))*dlogz);
        z2=zmin*pow(10.0,(abs(cord_z-(Nz/2))+1)*dlogz);
    }
    if(z<0.0){
        cord_z=(Nz/2)-int(log10(abs(z)/zmin)/dlogz)-1;
        if(cord_z<=0){
            cord_z=1;
        }
        z1=zmin*pow(10.0,(abs(cord_z-(Nz/2)+1))*dlogz);
        z2=zmin*pow(10.0,(abs(cord_z-(Nz/2)+1)+1)*dlogz);
    }
        
    if(cord_r>=Nr-1){
        cord_r=Nr-2;
    }

    if(cord_t>=Nt-1){
        cord_t=Nt-2;
    }

    r1=rmin*pow(10.0,cord_r*dlogr);
    r2=rmin*pow(10.0,(cord_r+1)*dlogr);
        
    t1=tmin*pow(10.0,cord_t*dlogt);
    t2=tmin*pow(10.0,(cord_t+1)*dlogt);
    
    double fE_cube[8];
    
    if(cord_z>=Nz/2){
        fE_cube[0]=fE_data[cord_z*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z,t)
        fE_cube[1]=fE_data[cord_z*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z,t)
        fE_cube[2]=fE_data[cord_z*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z,t+dt)
        fE_cube[3]=fE_data[cord_z*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z,t+dt)
        fE_cube[4]=fE_data[(cord_z+1)*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z+dz,t)
        fE_cube[5]=fE_data[(cord_z+1)*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z+dz,t)
        fE_cube[6]=fE_data[(cord_z+1)*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z+dz,t+dt)
        fE_cube[7]=fE_data[(cord_z+1)*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z+dz,t+dt)
    }
    if(cord_z<Nz/2){
        fE_cube[0]=fE_data[cord_z*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z,t)
        fE_cube[1]=fE_data[cord_z*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z,t)
        fE_cube[2]=fE_data[cord_z*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z,t+dt)
        fE_cube[3]=fE_data[cord_z*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z,t+dt)
        fE_cube[4]=fE_data[(cord_z-1)*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z-dz,t)
        fE_cube[5]=fE_data[(cord_z-1)*Nt*NE*Nr+cord_t*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z-dz,t)
        fE_cube[6]=fE_data[(cord_z-1)*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r];// f(E,r,z-dz,t+dt)
        fE_cube[7]=fE_data[(cord_z-1)*Nt*NE*Nr+(cord_t+1)*NE*Nr+cord_E*Nr+cord_r+1];// f(E,r+dr,z-dz,t+dt)
    }

    double fE_z1, fE_z2, fE;
    double fE1_z1, fE2_z1, fE1_z2, fE2_z2;
    double alpha1_z1, alpha2_z1, alpha1_z2, alpha2_z2;

    alpha1_z1=log(fE_cube[1]/fE_cube[0])/log(r2/r1);
    alpha2_z1=log(fE_cube[3]/fE_cube[2])/log(r2/r1);
    fE1_z1=fE_cube[0]*pow(r/r1,alpha1_z1);
    fE2_z1=fE_cube[2]*pow(r/r1,alpha2_z1);
    fE_z1=fE1_z1*pow(t/t1,log(fE2_z1/fE1_z1)/log(t2/t1));
    
    alpha1_z2=log(fE_cube[5]/fE_cube[4])/log(r2/r1);
    alpha2_z2=log(fE_cube[7]/fE_cube[6])/log(r2/r1);
    fE1_z2=fE_cube[4]*pow(r/r1,alpha1_z2);
    fE2_z2=fE_cube[6]*pow(r/r1,alpha2_z2);
    fE_z2=fE1_z2*pow(t/t1,log(fE2_z2/fE1_z2)/log(t2/t1));

    fE=fE_z1*pow(abs(z)/z1,log(fE_z2/fE_z1)/log(z2/z1));

    return fE;
}

int main(){
  
    // Calling the output file
    ifstream input, inputa;
    input.open("new_parameters.dat");
    
    double log10_Emin, log10_Emax, dlogE, dlogE_scale;
    double log10_zmin, log10_zSNR, log10_zmax, dlogz, dlogz_scale, log10_h, log10_hdisk;
    double log10_rmin, log10_rSNR, log10_rmax, dlogr, dlogr_scale;
    double log10_tmin, log10_tmax, dlogt, dlogt_scale;
    double sourceE, vA, rh, log10_zu, nH, zad, alpha;
    double ep_p, ep_e;
    int N_sample;
    string tag_D, tag_dist;

    // Read all the relevant inputs to define the grid and the model for CR sources
    input >> log10_Emin >> log10_Emax >> dlogE >> dlogE_scale;
    input >> log10_zmin >> log10_zSNR >> log10_zmax >> dlogz >> dlogz_scale >> log10_h >> log10_hdisk;
    input >> log10_rmin >> log10_rSNR >> log10_rmax >> dlogr >> dlogr_scale;
    input >> log10_tmin >> log10_tmax >> dlogt >> dlogt_scale;
    input >> sourceE >> vA;
    input >> N_sample;
    input >> rh >> log10_zu;
    input >> nH >> zad >> alpha;
    input >> ep_p >> ep_e;
    input >> tag_D >> tag_dist;

    tag_D="_"+tag_D;
    tag_dist="_"+tag_dist;

    dlogt=dlogt_scale, dlogz=dlogz_scale, dlogr=dlogr_scale, dlogE=dlogE_scale;

    // Define the grid for the Green's function
    double Emin=pow(10.0,log10_Emin), Emax=pow(10.0,log10_Emax);// eV
    double zmin=pow(10.0,log10_zmin), zmax=pow(10.0,log10_hdisk);// pc
    double rmin=pow(10.0,log10_rmin), rmax=pow(10.0,log10_rmax);// pc
    double tmin=pow(10.0,log10_tmin), tmax=pow(10.0,log10_tmax);// yr
    
    int Nt=int(log10(tmax/tmin)/dlogt)+1;
    int Nz=2*(int(log10(zmax/zmin)/dlogz)+1);
    int Nr=int(log10(rmax/rmin)/dlogr)+1;
    int NE=int(log10(Emax/Emin)/dlogE)+1;

    string tag_rh="_rh="+to_string(int(rh));
    string tag_vA="_vA="+to_string(int(vA));
    string tag_h="_h="+to_string(int(pow(10.0,log10_h)));
    string tag_nH="_nH="+to_string(int(nH*100.0));
    string tag_zu="_zu="+to_string(int(pow(10.0,log10_zu)));
    string tag_zad="_zad="+to_string(int(zad));
    string tag_alpha="_alpha="+to_string(int(alpha*100.0));
    string tag_ep="_ep="+to_string(int(ep_p*1000.0));
    string tag_source="_sourceE="+to_string(int(sourceE));
    string tag_sample="_"+to_string(int(N_sample));
    string tag="SNR";

    // Choose the right Green's function from the inputs of the model
    string name_ia="fEr_p_"+tag+tag_vA+tag_h+tag_nH+tag_zad+tag_D+tag_alpha+tag_ep+".dat";
    cout << "Reading the Green's function file "+name_ia << endl;

    // Open the Green's function file
    inputa.open(name_ia.c_str());

    // Create an array to store the tabulated Green's function
    int NfE=Nt*Nz*Nr*NE;
    double fE, *fE_data=new double[NfE];
    int count;

    // Read the Green's function file 
    count=0;
    while(inputa >> fE){
        fE_data[count]=fE;// eV^-1 cm^-3
        count++;
    }
    cout << "Number of lines in the Green's function file: " << endl;
    cout << "-> Estminated: " << count << endl;
    cout << "-> Expected:   " << NfE << endl;

    int l=12;
    double E=Emin*pow(10.0,l*dlogE);
    double r0=100.0, z0=20.0, t0=1.0e6;// pc, pc, yr

    cout << "Distribution function:" << endl; 
    cout << "r0 = " << r0 << " pc" << endl; 
    cout << "z0 = " << z0 << " pc" << endl; 
    cout << "t0 = " << t0 << " yr" << endl; 
    cout << "E =  " << E << " eV" << endl;
    cout << "f =  " << func_fE0_3D(fE_data,r0,z0,t0,l,rmin,zmin,tmin,dlogr,dlogz,dlogt,Nr,Nz,Nt,NE) << " eV^-1 cm^-3" << endl;    

    input.close();
    inputa.close();
    
    return 0;
}
